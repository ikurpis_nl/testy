var concat = {
  options: {
    stripBanners: true
  },
  js: {
    src: [
      "src/leaflet/leaflet-src.js",
      "src/lib/Leaflet.encoded/Polyline.encoded.js",
      "src/lib/PruneCluster/PruneCluster.js",
      "src/lib/jquery/jquery.js",
      "src/lib/jquery-ui/jquery-ui.js",
      "src/lib/momentjs/moment-with-locales.js",
      "src/lib/socket.io-client/socket.io.js",
      "src/lib/modernizr-load/modernizr.js",
      "src/ts/core/itemsService.js",
      "src/ts/core/directioinService.js",
      "src/ts/route/googleDirection.js",      
      "src/ts/list/list.js",
      "src/ts/list/listElement.js",
      "src/ts/list/jobOfferListElement.js",
      "src/ts/list/groupedList.js",
      "src/ts/list/groupedOfferListElement.js",
      "src/ts/list/groupedOfferListSubelement.js",
      "src/ts/server/server.js",
      "src/ts/map/map.js",
      "src/ts/map/routingDrawerLeaflet.js",
      "src/ts/virtuals/virtuals.js",
      "src/ts/virtuals/virtual.js",
      "src/ts/virtuals/jobOffer.js",
      "src/ts/markers/markers.js",
      "src/ts/markers/marker.js",
      "src/ts/filter/filter.js",
      "src/ts/filter/advancedFilter.js",
      "src/ts/navBar.js",
      "src/ts/navbars/categoriesNavBar.js",
      "src/ts/detailsNavBar.js",      
      "src/ts/events/events.js",
      "src/patches/momentjs-patch.js",
      "src/ts/utils.js",
      "src/ts/main.js"
    ],
    dest: 'dist/release/UP-src.js',
    nonull: true
  },

  css: {
    src: [
      'src/leaflet/leaflet.css',
      'src/lib/**/*.css',
      'src/sass/styles.css',
    ],
    dest: 'dist/release/css/UP-src.css',
    nonull: true
  }
}
module.exports = function (grunt) {
  grunt.config.set('concat', concat);
}
